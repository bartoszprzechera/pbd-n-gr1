USE pbd
GO

CREATE PROCEDURE CreateOrder ( @ClientId int, @CompanyClientId int, @Takeaway bit, @RestaurantId int, @OrderDate datetime, @NumberOfPersons int ,@TableId int)
AS
INSERT INTO Orders (ClientId, CompanyClientId, Takeaway, RestaurantId, OrderDate, NumberOfPersons, TableId) VALUES (@ClientId, @CompanyClientId, @Takeaway, @RestaurantId, @OrderDate, @NumberOfPersons,@TableId)
GO

CREATE PROCEDURE CreateClient ( @ClientId int, @ClientName varchar(50), @CompanyClientId int, @PhoneNumber varchar(12))
AS
INSERT INTO Client (ClientId, ClientName, CompanyClientId, PhoneNumber) VALUES (@ClientId, @ClientName, @CompanyClientId, @PhoneNumber)
GO

CREATE PROCEDURE CreateTable ( @TableId int, @Number int, @Capacity int, @RestaurantId int)
AS
INSERT INTO RestaurationTables (TableId, Number, Capacity, RestaurantId) VALUES (@TableId, @Number, @Capacity, @RestaurantId)
GO

CREATE PROCEDURE CreateCategory ( @CategoryId int, @CategoryName varchar(50), @CategoryDescription varchar(50))
AS
INSERT INTO Category (CategoryId, CategoryName, CategoryDescription) VALUES (@CategoryId, @CategoryName, @CategoryDescription)
GO

CREATE PROCEDURE AddDiscountItemToOrder (@OrderId int, @DiscountItemId int)
AS
UPDATE Orders
SET DiscountItemId = DiscountItemId
WHERE OrderId = OrderId;
GO

CREATE PROCEDURE AcceptReservation ( @ReservationId int)
AS
UPDATE Reservation
SET IsAccepted = 1
WHERE ReservationId = ReservationId;
GO