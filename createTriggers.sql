use pbd;
go
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE TRIGGER dbo.createDiscountItems
   ON  dbo.Orders
   AFTER INSERT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @ClientId int
	DECLARE @OrderId_inserted int
	DECLARE Order_inserted_CURSOR CURSOR
	LOCAL STATIC READ_ONLY FORWARD_ONLY
	FOR
	SELECT inserted.OrderId
	FROM inserted

	OPEN Order_inserted_CURSOR
	FETCH NEXT FROM Order_inserted_CURSOR INTO @OrderId_inserted
	WHILE @@FETCH_STATUS = 0
	BEGIN
		set @ClientId= (select inserted.ClientId from inserted where inserted.OrderId=@OrderId_inserted)
		if @ClientId is not Null
		begin
			DECLARE @DiscountId int

			DECLARE Discount_CURSOR CURSOR
			  LOCAL STATIC READ_ONLY FORWARD_ONLY
			FOR
			SELECT dbo.Discounts.DiscountId FROM dbo.Discounts where dbo.Discounts.IsActive=1

			OPEN Discount_CURSOR
			FETCH NEXT FROM Discount_CURSOR INTO @DiscountId
			WHILE @@FETCH_STATUS = 0
			BEGIN

				if (select dbo.Discounts.MinOrdersPrice from dbo.Discounts where dbo.Discounts.DiscountId=@DiscountId)<dbo.get_client_orders_price(@ClientId) and (select dbo.Discounts.MinOrdersAmount from dbo.Discounts where dbo.Discounts.DiscountId=@DiscountId)<dbo.get_client_orders_count(@ClientId)
				begin

					if(select count(dbo.DiscountItem.DiscountId) from dbo.DiscountItem where dbo.DiscountItem.ClientId=@ClientId and dbo.DiscountItem.DiscountId=@DiscountId) =0
					begin
						declare @StartingDate date
						set @StartingDate= (select inserted.OrderDate from inserted where inserted.OrderId=@OrderId_inserted)
						insert into dbo.DiscountItem (DiscountItemId,DiscountId,StartingDate,ClientId) VALUES (ISNULL((select top(1) DiscountItemId+1 from DiscountItem Order by DiscountItemId desc),1),@DiscountId,@StartingDate,@ClientId)


					end
				end

				FETCH NEXT FROM Discount_CURSOR INTO @DiscountId
			END
			CLOSE Discount_CURSOR
			DEALLOCATE Discount_CURSOR
		end
		FETCH NEXT FROM Order_inserted_CURSOR INTO @OrderId_inserted
	END
	CLOSE Order_inserted_CURSOR
	DEALLOCATE Order_inserted_CURSOR
END
GO