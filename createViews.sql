use pbd;
go
CREATE VIEW [dbo].[ClientOrder]
AS
SELECT dbo.Client.ClientId, dbo.Client.ClientName, dbo.Client.PhoneNumber, dbo.Product.ProductName, dbo.OrderItem.Quantity, dbo.Orders.Takeaway, dbo.RestaurationTables.TableId
FROM   dbo.Client INNER JOIN
             dbo.Orders ON dbo.Client.ClientId = dbo.Orders.ClientId INNER JOIN
             dbo.OrderItem ON dbo.Orders.OrderId = dbo.OrderItem.OrderId INNER JOIN
             dbo.Product ON dbo.OrderItem.ProductId = dbo.Product.ProductId INNER JOIN
             dbo.RestaurationTables ON dbo.Orders.TableId = dbo.RestaurationTables.TableId
GO

CREATE VIEW [dbo].[Reservations] AS
SELECT ClientName,PhoneNumber,Takeaway,ReservationDate
FROM Orders a, Reservation b, Client c
WHERE a.ClientId=c.ClientId
AND a.OrderId=b.OrderId;


GO

CREATE VIEW [dbo].[MenuList]
AS
SELECT dbo.Product.ProductName, dbo.Prices.NetPrice, dbo.Prices.StartDate, dbo.Prices.EndDate
FROM   dbo.Prices INNER JOIN
             dbo.Product ON dbo.Prices.ProductId = dbo.Product.ProductId
GO

CREATE VIEW [dbo].[InvoiceData]
AS
SELECT dbo.Client.ClientId, dbo.Client.ClientName, dbo.CompanyClient.CompanyName, dbo.CompanyClient.PhoneNumber, dbo.CompanyClient.City, dbo.CompanyClient.Street, dbo.CompanyClient.PostalCode, dbo.CompanyClient.NIP, dbo.Prices.NetPrice, dbo.Product.ProductName,
             dbo.Taxes.TaxType, dbo.Taxes.TaxPercentage
FROM   dbo.Product INNER JOIN
             dbo.Prices ON dbo.Product.ProductId = dbo.Prices.ProductId INNER JOIN
             dbo.Taxes ON dbo.Prices.TaxId = dbo.Taxes.TaxId CROSS JOIN
             dbo.Client INNER JOIN
             dbo.CompanyClient ON dbo.Client.CompanyClientId = dbo.CompanyClient.CompanyClientId
GO