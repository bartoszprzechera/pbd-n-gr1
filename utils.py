from random import randint
from datetime import date, timedelta

def daterange(start_date, end_date):
    for n in range(int((end_date - start_date).days)):
        yield start_date + timedelta(n)

def generate_random_nip():
    wagi =[6,5,7,2,3,4,5,6,7]
    cyfry=None
    cyfra_kontrolna=None
    while cyfra_kontrolna==10 or cyfra_kontrolna is None:
        cyfry = [randint(1, 9),randint(1, 9),randint(1, 9),randint(0, 9),randint(0, 9),randint(0, 9),randint(0, 9),randint(0, 9),randint(0, 9)]
        cyfra_kontrolna = 0
        for i in range(len(cyfry)):
            cyfra_kontrolna += wagi[i] * cyfry[i]
        cyfra_kontrolna%=11

    cyfry=[str(c) for c in cyfry]
    r ="".join(cyfry)
    r += str(cyfra_kontrolna)
    return r