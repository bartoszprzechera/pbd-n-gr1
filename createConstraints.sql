USE pbd
GO

ALTER TABLE MenuItem
ADD CONSTRAINT MenuItem_can_be_added CHECK (dbo.cant_item_be_added_to_menu(MenuItemId,MenuId)=0)
GO

ALTER TABLE Orders
ADD CONSTRAINT Orders_table_is_free CHECK (dbo.is_table_ready_for_order(RestaurantId,OrderDate,TableId)=1)
GO

ALTER TABLE OrderItem
ADD CONSTRAINT OrderItem_can_order_item_be_added_if_it_is_sea_fruit CHECK (dbo.can_order_item_be_added_if_it_is_sea_fruit(ProductId,OrderId)=1)
GO

ALTER TABLE CompanyClient
ADD CONSTRAINT NIP_CHECK CHECK (dbo.IsValidNip(NIP) = 1)
GO

ALTER TABLE Menu
ADD CONSTRAINT menu_data_collisions CHECK (dbo.cant_menu_be_added_data_collisions(RestaurantId,MenuStartDate,MenuEndDate) = 0)
GO

ALTER TABLE Prices
ADD CONSTRAINT price_data_collisions CHECK (dbo.cant_price_be_added_data_collisions(ProductId,StartDate,EndDate) = 0)
GO

ALTER TABLE Orders
ADD CONSTRAINT is_Discount_Usable CHECK (dbo.isDiscountItemUsableByClient(ClientId,OrderDate,DiscountItemId) = 1)
GO

ALTER TABLE OrderItem
ADD CONSTRAINT can_Order_Item_Be_Added CHECK (dbo.canOrderItemBeAdded(ProductId,OrderId) = 1)
GO

ALTER TABLE Orders
ADD CONSTRAINT num_pers_table_limit CHECK (dbo.isNumberOfPersonsLessThanTableLimit(TableId,NumberOfPersons) = 1)
GO

--ALTER TABLE RestaurationTables
--ADD CONSTRAINT restaurant_seats_limit CHECK (dbo.canTableBeAddedToResturant(Capacity,RestaurantId) = 1)
--GO

ALTER TABLE Reservation
ADD CONSTRAINT can_Client_Make_Reservation CHECK (dbo.canClientMakeReservation(OrderId) = 1)
GO