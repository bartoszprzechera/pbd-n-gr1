CREATE DATABASE pbd;
GO
USE pbd;
GO
DROP TABLE IF EXISTS Client;
GO
DROP TABLE IF EXISTS CompanyClient;
GO
DROP TABLE IF EXISTS Category;
GO
DROP TABLE IF EXISTS Menu;
GO
DROP TABLE IF EXISTS MenuItem;
GO
DROP TABLE IF EXISTS Orders;
GO
DROP TABLE IF EXISTS Product;
GO
DROP TABLE IF EXISTS Restaurant;
GO
DROP TABLE IF EXISTS DiscountItem;
GO
DROP TABLE IF EXISTS Discounts;
GO
DROP TABLE IF EXISTS OrderItem;
GO
DROP TABLE IF EXISTS Prices;
GO
DROP TABLE IF EXISTS Reservation;
GO
DROP TABLE IF EXISTS RestaurationTables;
GO
DROP TABLE IF EXISTS Taxes;
GO



CREATE TABLE CompanyClient
(
 CompanyClientId int NOT NULL ,
 CompanyName            varchar(50) NOT NULL ,
 PhoneNumber     varchar(12) unique NOT NULL ,
 City            varchar(40) NOT NULL ,
 Street          varchar(50) NOT NULL ,
 PostalCode      varchar(6) NOT NULL ,
 NIP             nvarchar(15) unique NOT NULL,

 CONSTRAINT PK_companyclient PRIMARY KEY CLUSTERED (CompanyClientId ASC),
);
GO

CREATE TABLE Client
(
 ClientId        int NOT NULL ,
 ClientName            varchar(50) NOT NULL ,
 CompanyClientId int NULL ,
 PhoneNumber     varchar(12) unique NOT NULL ,

 CONSTRAINT PK_client PRIMARY KEY CLUSTERED (ClientId ASC),
 CONSTRAINT FK_223 FOREIGN KEY (CompanyClientId)  REFERENCES CompanyClient(CompanyClientId)
);
GO


CREATE TABLE Category
(
 CategoryId int NOT NULL  ,
 CategoryName       varchar(50) NOT NULL ,
 CategoryDescription       varchar(50) NOT NULL ,

 CONSTRAINT PK_category PRIMARY KEY CLUSTERED (CategoryId ASC)
);
GO

CREATE TABLE Restaurant
(
 RestaurantId        int NOT NULL ,
 maximalPeopleAtOnce int NOT NULL ,

 CONSTRAINT PK_restaurants PRIMARY KEY CLUSTERED (RestaurantId ASC)
);
GO
CREATE TABLE Menu
(
 MenuId       int NOT NULL ,
 RestaurantId int NOT NULL ,
 MenuStartDate         date NOT NULL ,
 MenuEndDate         date NOT NULL ,

 CONSTRAINT PK_menu PRIMARY KEY CLUSTERED (MenuId ASC),
 CONSTRAINT FK_146 FOREIGN KEY (RestaurantId)  REFERENCES Restaurant(RestaurantId)
);
GO

 CREATE TABLE Product
(
 ProductId   int NOT NULL ,
 ProductName varchar(50) NOT NULL ,
 CategoryId  int NOT NULL ,

 CONSTRAINT PK_Product PRIMARY KEY CLUSTERED (ProductId ASC),
 CONSTRAINT AK1_Product_SupplierId_ProductName UNIQUE NONCLUSTERED (ProductName ASC),
 CONSTRAINT FK_164 FOREIGN KEY (CategoryId)  REFERENCES Category(CategoryId)
);
GO

CREATE TABLE MenuItem
(
 MenuItemId int NOT NULL ,
 MenuId     int NOT NULL ,
 ProductId  int NOT NULL ,

 CONSTRAINT PK_menuitem PRIMARY KEY CLUSTERED (MenuItemId ASC),
 CONSTRAINT FK_154 FOREIGN KEY (MenuId)  REFERENCES Menu(MenuId),
 CONSTRAINT FK_157 FOREIGN KEY (ProductId)  REFERENCES Product(ProductId)
);
GO



CREATE TABLE Discounts
(
 DiscountId        int NOT NULL ,
 RestaurantId      int NOT NULL ,
 MinOrdersAmount   int DEFAULT 1 ,
 MinOrdersPrice    float DEFAULT 1 ,
 DiscountPercent           float NOT NULL ,
 NumOfDaysToUse    int DEFAULT -1,
 IsOneTime         bit DEFAULT 1,
 IsActive          bit DEFAULT 1,

 CONSTRAINT PK_discounts PRIMARY KEY CLUSTERED (DiscountId ASC),
 CONSTRAINT FK_324 FOREIGN KEY (RestaurantId)  REFERENCES Restaurant(RestaurantId)
);
GO

CREATE TABLE DiscountItem
(
 DiscountItemId int NOT NULL ,
 DiscountId     int NOT NULL ,
 StartingDate   date NULL ,
 ClientId        int NOT NULL ,

 CONSTRAINT PK_discountItem PRIMARY KEY CLUSTERED (DiscountItemId ASC),
 CONSTRAINT FK_340 FOREIGN KEY (DiscountId)  REFERENCES Discounts(DiscountId)
);
GO

CREATE TABLE RestaurationTables
(
 TableId      int NOT NULL ,
 Number       int NOT NULL ,
 Capacity     int NOT NULL ,
 RestaurantId int NOT NULL ,

 CONSTRAINT PK_table PRIMARY KEY CLUSTERED (TableId ASC),
 CONSTRAINT FK_260 FOREIGN KEY (RestaurantId)  REFERENCES Restaurant(RestaurantId)
);
GO


CREATE TABLE Orders
(
 OrderId        int NOT NULL ,
 ClientId       int NULL,
 CompanyClientId int Null,
 Takeaway       bit NOT NULL ,
 RestaurantId   int NOT NULL ,
 OrderDate           datetime NOT NULL ,
 NumberOfPersons int DEFAULT 1,
 DiscountItemId int NULL ,
 TableId        int NULL,

 CONSTRAINT FK_304_1 FOREIGN KEY (CompanyClientId)  REFERENCES CompanyClient(CompanyClientId),
 CONSTRAINT PK_order PRIMARY KEY CLUSTERED (OrderId ASC),
 CONSTRAINT FK_177 FOREIGN KEY (ClientId)  REFERENCES Client(ClientId),
 CONSTRAINT FK_266 FOREIGN KEY (RestaurantId)  REFERENCES Restaurant(RestaurantId),
 CONSTRAINT FK_56766 FOREIGN KEY (DiscountItemId)  REFERENCES DiscountItem(DiscountItemId),
 CONSTRAINT FK_56763 FOREIGN KEY (TableId)  REFERENCES RestaurationTables(TableId)
);
GO


CREATE TABLE OrderItem
(
 OrderItemId int NOT NULL ,
 ProductId   int NOT NULL ,
 Quantity    int NOT NULL ,
 OrderId     int NOT NULL ,

 CONSTRAINT PK_orderitem PRIMARY KEY CLUSTERED (OrderItemId ASC),
 CONSTRAINT FK_195 FOREIGN KEY (ProductId)  REFERENCES Product(ProductId),
 CONSTRAINT FK_202 FOREIGN KEY (OrderId)  REFERENCES Orders(OrderId)
);
GO


 CREATE TABLE Taxes
(
 TaxId      int NOT NULL ,
 TaxType       varchar(50) NOT NULL ,
 TaxPercentage int NOT NULL ,


 CONSTRAINT PK_taxes PRIMARY KEY CLUSTERED (TaxId ASC)
);
GO

CREATE TABLE Prices
(
 ProductPriceId int NOT NULL ,
 NetPrice       FLOAT NOT NULL ,
 TaxId          int NOT NULL ,
 StartDate      date NOT NULL ,
 EndDate        date NOT NULL ,
 ProductId      int NULL ,

 CONSTRAINT PK_productPrices PRIMARY KEY CLUSTERED (ProductPriceId ASC),
 CONSTRAINT FK_289 FOREIGN KEY (TaxId)  REFERENCES Taxes(TaxId),
 CONSTRAINT FK_312 FOREIGN KEY (ProductId)  REFERENCES Product(ProductId)
);
GO

CREATE TABLE Reservation
(
 ReservationId int NOT NULL ,
 OrderId       int NOT NULL ,
 ReservationDate datetime NOT NULL ,
 IsAccepted    bit NOT NULL DEFAULT 0,
 AlreadyPaid   bit DEFAULT 0,

 CONSTRAINT PK_reservation PRIMARY KEY CLUSTERED (ReservationId ASC),
 CONSTRAINT FK_211 FOREIGN KEY (OrderId)  REFERENCES Orders(OrderId)
);
GO
CREATE NONCLUSTERED INDEX fkIdx_289 ON Prices
 (
  TaxId ASC
 )
 GO
 CREATE NONCLUSTERED INDEX fkIdx_312 ON Prices
 (
  ProductId ASC
 )
 GO

CREATE NONCLUSTERED INDEX fkIdx_212 ON Reservation 
 (
  OrderId ASC
 )
 GO
 CREATE NONCLUSTERED INDEX fkIdx_196 ON OrderItem
 (
  ProductId ASC
 )
 GO

CREATE NONCLUSTERED INDEX fkIdx_203 ON OrderItem
 (
  OrderId ASC
 )
 GO

CREATE NONCLUSTERED INDEX fkIdx_178 ON Orders
 (
  ClientId ASC
 )
 GO
CREATE NONCLUSTERED INDEX fkIdx_o_cc ON Orders
 (
  CompanyClientId ASC
 )
 GO

CREATE NONCLUSTERED INDEX fkIdx_267 ON Orders
 (
  RestaurantId ASC
 )
 GO
 CREATE NONCLUSTERED INDEX fkIdx_o_di ON Orders
 (
  DiscountItemId ASC
 )
 GO
  CREATE NONCLUSTERED INDEX fkIdx_o_ti ON Orders
 (
  TableId ASC
 )
 GO

CREATE NONCLUSTERED INDEX fkIdx_261 ON RestaurationTables
 (
  RestaurantId ASC
 )
 GO


CREATE NONCLUSTERED INDEX fkIdx_341 ON DiscountItem
 (
  DiscountId ASC
 )
 GO

CREATE NONCLUSTERED INDEX fkIdx_325 ON Discounts
 (
  RestaurantId ASC
 )
 GO

CREATE NONCLUSTERED INDEX fkIdx_155 ON MenuItem
 (
  MenuId ASC
 )
 GO


CREATE NONCLUSTERED INDEX fkIdx_158 ON MenuItem
 (
  ProductId ASC
 )
 GO

CREATE NONCLUSTERED INDEX fkIdx_165 ON Product
 (
  CategoryId ASC
 )
 GO

CREATE NONCLUSTERED INDEX fkIdx_m_restid ON Menu
 (
  RestaurantId ASC
 )
 GO

CREATE NONCLUSTERED INDEX fkIdx_224 ON Client
 (
  CompanyClientId ASC
 )
 GO
