import pyodbc
from barnum import gen_data
from random import randint, choice
import datetime
from utils import generate_random_nip, daterange

cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};'
                      'Server=localhost,1433;'
                      'Database=pbd;'
                      'UID=SA;'
                      'PWD=zaq1@WSX;'
                      'Trusted_Connection=no;')
company_clients = []
clients = []
restaurants = []
tables = []
categories = []
taxes = []
products = []
menus = []
menu_items = []
prices = []
discounts = []
discount_items = []
orders = []
order_items = []
reservations = []

cnxn.setdecoding(pyodbc.SQL_CHAR, encoding='utf-8')
cnxn.setdecoding(pyodbc.SQL_WCHAR, encoding='utf-8')
cnxn.setencoding(encoding='utf-8')
cursor = cnxn.cursor()


def insert(table, names, values, hmao=1000):
    for i in range(max(1, int(len(values) / hmao)) + 1):
        try:
            name_string = "(" + ", ".join(names) + ")"
            values_string = ", ".join(["('" + "', '".join([str(dp) for dp in data_point]) + "')" for data_point in
                                       values[max(0, (i) * hmao):(i + 1) * hmao]])
            # print(name_string,values_string)
            if values_string == "":
                break
            sql = "INSERT INTO " + table + " " + name_string + " VALUES " + values_string
            print(str(sql[:300]))
            cursor.execute(sql)
            cnxn.commit()
        except Exception as e:
            print(e)


def update(table, names, values, id_name, id):
    try:
        names_values_string = ", ".join([str(n) + " = " + str(v) for n, v in zip(names, values)])
        sql = "UPDATE " + table + " SET " + names_values_string + " WHERE " + str(id_name) + " = " + str(id)
        print(sql)
        cursor.execute(sql)
        cnxn.commit()
    except Exception as e:
        print(e)


def generateCompanyClients(num=100):
    for i in range(1, num + 1):
        name = gen_data.create_company_name()
        phone = str(gen_data.create_phone().replace("-", "").replace("(", "").replace(")", ""))
        postal_code, city, _ = gen_data.create_city_state_zip()
        postal_code = str(postal_code)
        street = str(gen_data.create_street())
        nip = str(generate_random_nip())
        company_clients.append([i, name, phone, city, street, postal_code, nip])
    insert("CompanyClient", ["CompanyClientId", "CompanyName", "PhoneNumber", "City", "Street", "PostalCode", "NIP"],
           company_clients)
    pass


def generateClients(num=100, num_in_company=(1, 5)):
    min_num_in_company, max_num_in_company = num_in_company

    i = 1
    for company in company_clients:
        num_clients = randint(min_num_in_company, max_num_in_company)
        for _ in range(num_clients):
            name, forename = gen_data.create_name()
            name += " " + forename
            phone = str(gen_data.create_phone().replace("-", "").replace("(", "").replace(")", ""))
            clients.append([i, name, company[0], phone])
            i += 1
    insert("Client", ["ClientId", "ClientName", "CompanyClientId", "PhoneNumber"], clients)

    x = i - 1
    for _ in range(num):
        name, forename = gen_data.create_name()
        name += " " + forename
        phone = str(gen_data.create_phone().replace("-", "").replace("(", "").replace(")", ""))
        clients.append([i, name, None, phone])
        i += 1
    clients_to_insert = clients[x:]
    clients_to_insert = [list(filter(lambda a: a is not None, client)) for client in clients_to_insert]
    print(len(clients))
    insert("Client", ["ClientId", "ClientName", "PhoneNumber"], clients_to_insert)


def generateRestaurants(num=5):
    for i in range(1, num + 1):
        maximalPeopleAtOnce = randint(10, 40)
        restaurants.append([i, maximalPeopleAtOnce])
    insert("Restaurant", ["RestaurantId", "maximalPeopleAtOnce"], restaurants)


def generateTables():
    i = 1
    for restaurant in restaurants:
        curr_cap = restaurant[1]
        x = 1
        while curr_cap != 0:
            number = x
            capacity = randint(1, min(6, curr_cap))
            tables.append([i, number, capacity, restaurant[0]])
            curr_cap -= capacity
            i += 1
            x += 1
    insert("RestaurationTables", ["TableId", "Number", "Capacity", "RestaurantId"], tables)


def generateCategories(num=10):
    categories.append([1, "Owoce Morza", "Owoce morza desc bla bla bla."])
    for i in range(2, num + 1):
        name = gen_data.create_nouns(1)
        desc = gen_data.create_nouns(4)
        categories.append([i, name, desc])
    insert("Category", ["CategoryId", "CategoryName", "CategoryDescription"], categories)


def createTaxes(num=6):
    for i in range(1, num + 1):
        TaxType = gen_data.create_nouns(4)
        TaxPercentage = randint(0, 30)
        taxes.append([i, TaxType, TaxPercentage])
    insert("Taxes", ["TaxId", "TaxType", "TaxPercentage"], taxes)


def createProducts(numProductsForCategory=(10, 15)):
    i = 1
    min_numProductsForCategory, max_numProductsForCategory = numProductsForCategory
    for category in categories:
        npfc = randint(min_numProductsForCategory, max_numProductsForCategory)
        for _ in range(npfc):
            ProductName = gen_data.create_nouns(4)
            products.append([i, ProductName, category[0]])
            i += 1
    insert("Product", ["ProductId", "ProductName", "CategoryId"], products)


def createMenus(start_date=datetime.date(2013, 1, 1), end_date=datetime.date(2015, 6, 2)):
    i = 1
    for restaurant in restaurants:
        skip = 0
        br = False
        for date in daterange(start_date, end_date):
            if skip > 0:
                skip -= 1
                continue
            menu_days = randint(7, 14)
            menu_end_date = date + datetime.timedelta(days=menu_days)
            if menu_end_date > end_date:
                menu_end_date = end_date
                br = True
            menu_end_date += datetime.timedelta(hours=23, minutes=59, seconds=59)
            skip = menu_days
            # print(date, date + datetime.timedelta(days=menu_days))
            menus.append([i, restaurant[0], date, menu_end_date])
            i += 1
            if br:
                break
    insert("Menu", ["MenuId", "RestaurantId", "MenuStartDate", "MenuEndDate"], menus)


def last_menu_contains_product(last_menu_id, product_id):
    if len(menu_items) == 0:
        return False
    for item in menu_items:
        if item[1] == last_menu_id and item[2] == product_id:
            return True
    return False


def createMenuItems():
    i = 1
    for menu_i, menu in enumerate(menus):
        menuLenght = randint(20, 30)
        for _ in range(menuLenght):
            random_products = [0, 0, 1]

            if menu_i == 0:
                while random_products[2] == 1 \
                        or last_menu_contains_product(menu[0], random_products[0]):
                    random_products = choice(products)
                    # print(random_products)
            elif menu_i == 1:
                while random_products[2] == 1 \
                        or last_menu_contains_product(menus[menu_i - 1][0], random_products[0]) \
                        or last_menu_contains_product(menu[0], random_products[0]):
                    random_products = choice(products)
            else:
                while random_products[2] == 1 \
                        or last_menu_contains_product(menus[menu_i - 2][0], random_products[0]) \
                        or last_menu_contains_product(menus[menu_i - 1][0], random_products[0]) \
                        or last_menu_contains_product(menu[0], random_products[0]):
                    random_products = choice(products)
                    # print(random_products)
            menu_items.append([i, menu[0], random_products[0]])
            i += 1
    # print(menu_items)
    insert("MenuItem", ["MenuItemId", "MenuId", "ProductId"], menu_items)


def createPrices(start_date=datetime.date(2013, 1, 1), end_date=datetime.date(2015, 6, 2)):
    i = 0
    for product in products:
        product_i = 0
        skip = 0
        br = False
        for date in daterange(start_date, end_date):

            if skip > 0:
                skip -= 1
                continue
            price_days = randint(2, 31)
            price_end_date = date + datetime.timedelta(days=price_days)
            if price_end_date > end_date:
                price_end_date = end_date
                br = True
            price_end_date += datetime.timedelta(hours=23, minutes=59, seconds=59)
            skip = price_days

            if product_i == 0:
                NetPrice = randint(100, 6000) / 100.0
                TaxId = choice(taxes)[0]
            else:
                NetPrice = prices[-1][1] if randint(0, 1000) <= 500 else prices[-1][1] + randint(-100, 100) / 100.0
                if NetPrice < 1.0:
                    NetPrice = 1.0
                    NetPrice += randint(100, 200) / 100.0
                TaxId = prices[-1][2] if randint(0, 1000) <= 990 else choice(taxes)[0]
                # print(NetPrice)
            prices.append([i, NetPrice, TaxId, date, price_end_date, product[0]])
            product_i += 1
            i += 1
            if br:
                break
    insert("Prices", ["ProductPriceId", "NetPrice", "TaxId", "StartDate", "EndDate", "ProductId"], prices)


def createDiscounts(num=3):
    i = 1
    for restaurant in restaurants:
        for _ in range(num):
            MinOrdersAmount = randint(1, 20)
            MinOrdersPrice = randint(10, 1000)
            NumOfDaysToUse = -1 if randint(0, 100) < 50 else randint(7, 31)
            IsOneTime = 1 if randint(0, 100) < 66 or NumOfDaysToUse != -1 else 0
            DiscountPercent = randint(1, 25) if IsOneTime == 1 else randint(1, 6)
            discounts.append(
                [i, restaurant[0], MinOrdersAmount, MinOrdersPrice, DiscountPercent, NumOfDaysToUse, IsOneTime])
            i += 1
    insert("Discounts",
           ["DiscountId", "RestaurantId", "MinOrdersAmount", "MinOrdersPrice", "DiscountPercent", "NumOfDaysToUse",
            "IsOneTime"], discounts)


def getFreeTablesInRestaurant(restaurantId, time):
    tables_in_restaurant = list(filter(lambda t: t[3] == restaurantId, tables))
    tables_in_restaurant = sorted(tables_in_restaurant)
    orders_in_restaurant = list(filter(lambda t: t[4] == restaurantId and t[3] == 0, orders))
    orders_in_time = list(
        filter(lambda t: time - datetime.timedelta(minutes=95) <= t[5] <= time + datetime.timedelta(minutes=95),
               orders_in_restaurant))
    # if len(orders_in_time) != 0:
    #     print(tables_in_restaurant)
    for order in orders_in_time:
        tables_in_restaurant = list(filter(lambda t: t[0] != order[7], tables_in_restaurant))
    # if len(orders_in_time) != 0:
    #     print(tables_in_restaurant,orders_in_time)
    return tables_in_restaurant


# ["OrderId", "ClientId", "Takeaway", "RestaurantId", "OrderDate", "NumberOfPersons"]
# tables = [[1, 1, 1, 1], [2, 2, 1, 1], [3, 3, 2, 1], [4, 4, 2, 1], [5, 5, 4, 1], [6, 6, 4, 1]]
# tt = datetime.datetime.now() - datetime.timedelta(minutes=30)
# tt2 = datetime.datetime.now() - datetime.timedelta(hours=30)
# orders = [[0, 1, 0, 1, tt2, 4], [1, 1, 0, 1, tt, 4], [2, 2, 0, 1, tt, 1], [3, 3, 0, 1, tt, 2], [4, 4, 0, 1, tt, 2]]
# ft = getFreeTablesInRestaurant(1, datetime.datetime.now())
#
# print(ft)


def createOrders(start_date=datetime.date(2013, 1, 1), end_date=datetime.date(2015, 6, 2)):
    i = 1

    for client in clients:
        ci = 0
        clientType = 3 if randint(1, 100) > 50 else 2 if randint(1, 100) > 50 else 1
        skip = 0
        csd = start_date + datetime.timedelta(days=randint(0, (end_date - start_date).days - 31))
        for date in daterange(csd, end_date):
            if skip > 0:
                skip -= 1
                continue
            if clientType == 3:
                skip = randint(1, 3)
            elif clientType == 2:
                skip = randint(3, 7)
            else:
                skip = randint(7, 30)

            if ci < 2 or randint(0, 100) > 95:
                restaurant = choice(restaurants)[0]
            elif randint(0, 100) > 20:
                restaurant = orders[i - ci - 1][3]
            else:
                restaurant = orders[i - ci][3]
            if randint(0, 100) > 99:
                break
            takeaway = 1 if randint(1, 10) > 7 else 0

            order_date = datetime.datetime.combine(date, datetime.time(randint(9, 21), randint(0, 59), 0, 0))
            free_tables = getFreeTablesInRestaurant(restaurant, order_date)
            # print(free_tables,restaurant)
            if len(free_tables) == 0:
                continue
            table = choice(free_tables) if takeaway == 0 else None
            number_of_peaples = table[2] if table is not None else 0
            if table is not None:
                table = table[0]
            orders.append([i, client[0], None, takeaway, restaurant, order_date, number_of_peaples, table])
            i += 1
            ci += 1
    for client in company_clients:
        ci = 0
        clientType = 3 if randint(1, 100) > 50 else 2 if randint(1, 100) > 50 else 1
        skip = 0
        csd = start_date + datetime.timedelta(days=randint(0, (end_date - start_date).days - 31))
        for date in daterange(csd, end_date):
            if skip > 0:
                skip -= 1
                continue
            if clientType == 3:
                skip = randint(0, 3)
            elif clientType == 2:
                skip = randint(3, 14)
            else:
                skip = randint(14, 30)

            if ci < 2 or randint(0, 100) > 95:
                restaurant = choice(restaurants)[0]
            elif randint(0, 100) > 20:
                restaurant = orders[i - ci - 1][3]
            else:
                restaurant = orders[i - ci][3]
            if randint(0, 100) > 99:
                break
            takeaway = 1 if randint(1, 10) > 2 else 0

            order_date = datetime.datetime.combine(date, datetime.time(randint(9, 21), randint(0, 59), 0, 0))
            free_tables = getFreeTablesInRestaurant(restaurant, order_date)
            # print(free_tables,restaurant)
            if len(free_tables) == 0:
                continue
            table = choice(free_tables) if takeaway == 0 else None
            number_of_peaples = table[2] if table is not None else 0
            if table is not None:
                table = table[0]
            orders.append([i, None, client[0], takeaway, restaurant, order_date, number_of_peaples, table])
            i += 1
            ci += 1

    for date in daterange(start_date, end_date):
        for restaurant in restaurants:
            for _ in range(randint(5, 10)):
                takeaway = 1 if randint(1, 10) > 2 else 0
                order_date = datetime.datetime.combine(date, datetime.time(randint(9, 21), randint(0, 59), 0, 0))
                free_tables = getFreeTablesInRestaurant(restaurant[0], order_date)
                if len(free_tables) == 0:
                    continue
                table = choice(free_tables) if takeaway == 0 else None
                number_of_peaples = table[2] if table is not None else 0
                if table is not None:
                    table = table[0]
                # print([i, None, takeaway, restaurant, order_date, number_of_peaples])
                orders.append([i, None, None, takeaway, restaurant[0], order_date, number_of_peaples, table])
                i += 1
    print(len(orders))
    print(len(list(filter(lambda o: o[1] is not None, orders))))
    print(len(list(filter(lambda o: o[1] is None, orders))))
    insert("Orders", ["OrderId", "ClientId", "Takeaway", "RestaurantId", "OrderDate", "NumberOfPersons", "TableId"],
           [list(filter(lambda a: a is not None, order)) for order in
            list(filter(lambda o: o[1] is not None and o[7] is not None, orders))])
    insert("Orders",
           ["OrderId", "CompanyClientId", "Takeaway", "RestaurantId", "OrderDate", "NumberOfPersons", "TableId"],
           [list(filter(lambda a: a is not None, order)) for order in
            list(filter(lambda o: o[2] is not None and o[7] is not None, orders))])
    insert("Orders", ["OrderId", "Takeaway", "RestaurantId", "OrderDate", "NumberOfPersons", "TableId"],
           [list(filter(lambda a: a is not None, order)) for order in
            list(filter(lambda o: o[1] is None and o[2] is None and o[7] is not None, orders))])

    insert("Orders", ["OrderId", "ClientId", "Takeaway", "RestaurantId", "OrderDate", "NumberOfPersons"],
           [list(filter(lambda a: a is not None, order)) for order in
            list(filter(lambda o: o[1] is not None and o[7] is None, orders))])
    insert("Orders",
           ["OrderId", "CompanyClientId", "Takeaway", "RestaurantId", "OrderDate", "NumberOfPersons"],
           [list(filter(lambda a: a is not None, order)) for order in
            list(filter(lambda o: o[2] is not None and o[7] is None, orders))])
    insert("Orders", ["OrderId", "Takeaway", "RestaurantId", "OrderDate", "NumberOfPersons"],
           [list(filter(lambda a: a is not None, order)) for order in
            list(filter(lambda o: o[1] is None and o[2] is None and o[7] is None, orders))])


def isProcutAvialibleInTime(product_id, restaurant_id, time=datetime.date.today()):
    menu_from_restaurant_in_time = list(filter(lambda m: m[1] == restaurant_id and m[2] <= time <= m[3], menus))
    if len(menu_from_restaurant_in_time) == 0:
        return False
    selected_menu = menu_from_restaurant_in_time[0]
    menu_items_from_selected_menu_that_match = list(
        filter(lambda mi: mi[1] == selected_menu[0] and mi[2] == product_id, menu_items))
    if len(menu_items_from_selected_menu_that_match) == 0:
        return False
    return True


def list_products_avialible_in_time(restaurant_id, time=datetime.date.today()):
    menu_from_restaurant_in_time = list(filter(lambda m: m[1] == restaurant_id and m[2] <= time <= m[3], menus))
    if len(menu_from_restaurant_in_time) == 0:
        return None
    selected_menu = menu_from_restaurant_in_time[0]
    menu_items_from_selected_menu_that_match = list(
        filter(lambda mi: mi[1] == selected_menu[0], menu_items))
    res = []
    for mi in menu_items_from_selected_menu_that_match:
        res.append(mi[2])
    return res

def createOrderItems():
    i = 1
    for order in orders:
        num_products = randint(max(order[6], 1), max(1, order[6]) * 2)
        order_products_ids = []
        for _ in range(num_products):
            av_p = list_products_avialible_in_time(order[4], order[5].date())
            if av_p is None:
                print("av_p is None")
                continue
            product_id = choice(av_p)
            if product_id is None:
                continue
            product = list(filter(lambda p: p[0] == product_id, products))[0]
            while product[0] in order_products_ids:
                product_id = choice(list_products_avialible_in_time(order[4], order[5].date()))
                product = list(filter(lambda p: p[0] == product_id, products))[0]
            order_products_ids.append(product[0])
            quantity = 1 if randint(0, 100) > 30 else 2 if randint(0, 100) > 30 else 3
            order_items.append([i, product[0], quantity, order[0]])
            i += 1
    insert("OrderItem", ["OrderItemId", "ProductId", "Quantity", "OrderId"], order_items)


def getProductPrice(product_id, date=datetime.date.today()):
    # print(type(prices[0][3]),type(date.date()),type(prices[0][4]))
    price = list(filter(lambda p: p[5] == product_id and p[3] <= date.date() <= p[4], prices))[0]
    tax = list(filter(lambda t: t[0] == price[2], taxes))[0]
    # print("price: "+str(price[1] *(1+tax[2]/100.0)))
    return price[1] * (1 + tax[2] / 100.0)


def getOrderPriceWithoutDiscounts(orderId, date=datetime.date.today()):
    items = list(filter(lambda oi: oi[3] == orderId, order_items))
    sum = 0
    for item in items:
        sum += item[2] * getProductPrice(item[1], date)
        # print("price: "+str(getProductPrice(item[1],date))+", quaintity: "+str(item[2]) )
    # print("sum: "+str(sum))
    return sum


def canClientMakeResevation(clientId, isCompany, time=datetime.datetime.now()):
    if clientId is None:
        return False
    if isCompany:
        id_num = 2
    else:
        id_num = 1
    client_order_before = list(filter(lambda o: o[id_num] == clientId and o[5] < time, orders))
    if len(client_order_before) >= 5:
        return True
    sum = 0
    for order in client_order_before:
        sum += getOrderPriceWithoutDiscounts(order[0], time)
        if sum >= 200:
            return True
    return False


def getOrderTable(order):
    tables_in_restaurant = list(filter(lambda t: t[3] == order[4], tables))

    num_seats_to_end = order[6]
    selected_table = None
    for i, table in enumerate(tables_in_restaurant):
        if table[2] >= num_seats_to_end:
            if selected_table is None or selected_table[2] > table[2]:
                selected_table = table
    if selected_table is None:
        print("selected_table is None")
        return None
    return selected_table


def createReservations():
    i = 1
    for order in orders:
        if randint(0, 100) < 85:
            continue
        reservation_date = order[5] - datetime.timedelta(days=randint(5, 14), hours=randint(0, 23),
                                                         minutes=randint(0, 59))
        if order[1] is not None and order[2] is None and not canClientMakeResevation(order[1], False, order[
            5]) and getOrderPriceWithoutDiscounts(order[0], order[5]) < 50:
            # print("asd")
            continue
        is_accepted = 1
        AlreadyPaid = 1 if randint(0, 10) > 5 else 0

        reservations.append([i, order[0], reservation_date, is_accepted, AlreadyPaid])
        i += 1
    # print(reservations)
    insert("Reservation",
           ["ReservationId", "OrderId", "ReservationDate", "IsAccepted", "AlreadyPaid"],
           reservations, 1)


def createDiscountItems():
    i = 1
    for client in clients:
        for discount in discounts:
            client_orders = list(filter(lambda o: o[1] == client[0], orders))
            order_money = 0
            order_count = 0
            date = None
            for order in client_orders:
                if discount[1] == order[4] and discount[3] <= order_money and discount[2] <= order_count:
                    discount_items.append([i, discount[0], date, client[0]])
                    i += 1
                    if discount[6] == 0:
                        break
                    else:
                        order_count = 0
                        order_money = 0

                date = order[5]
                order_count += 1
                order_money += getOrderPriceWithoutDiscounts(order[0], order[5])
    insert("DiscountItem",
           ["DiscountItemId", "DiscountId", "StartingDate", "ClientId"],
           discount_items)


def getDiscountItemDiscount(discountItemId):
    x = list(filter(lambda d: d[0] == discountItemId, discounts))
    return x[0] if len(x) > 0 else None


def assignDiscountItemsToOrders():
    # to_update=[]
    for client in clients:
        client_discounts = list(filter(lambda di: di[3] == client[0], discount_items))
        client_orders = list(filter(lambda o: o[1] == client[0], orders))
        for order in client_orders:
            discountItemsThatUserCanUse = list(
                filter(lambda di: getDiscountItemDiscount(di[1]) is not None, client_discounts))
            discountItemsThatUserCanUse = list(filter(lambda di: di[2] < order[5] and (
                        getDiscountItemDiscount(di[1])[5] == -1 or (
                            di[2] + datetime.timedelta(days=getDiscountItemDiscount(di[1])[5])) >= order[5]),
                                                      discountItemsThatUserCanUse))
            if len(discountItemsThatUserCanUse) == 0:
                continue
            discountItemsThatUserCanUse.sort(key=lambda x: getDiscountItemDiscount(x[1])[4])
            moastValuableDiscountItem = discountItemsThatUserCanUse[0]
            if getDiscountItemDiscount(moastValuableDiscountItem[1])[6] == 1:
                client_discounts.remove(moastValuableDiscountItem)
            update("Orders", ["DiscountItemId"], [moastValuableDiscountItem[0]], "OrderId", order[0])


start_date = datetime.date(2021, 1, 1)
end_date = datetime.date(2022, 6, 2)
generateCompanyClients()
generateClients()
generateRestaurants()
generateTables()
generateCategories()
createTaxes()
createProducts()
createMenus(start_date, end_date)
createMenuItems()
createPrices(start_date, end_date)
createDiscounts()
createOrders(start_date, end_date)
createOrderItems()
createReservations()
createDiscountItems()
assignDiscountItemsToOrders()
