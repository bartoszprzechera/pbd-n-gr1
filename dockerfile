FROM mcr.microsoft.com/mssql/server:2019-latest
USER root
RUN apt-get update -y && apt-get install -y python3 python3-pip && apt-get install -y unixodbc-dev && pip3 install pyodbc barnum
ENV ACCEPT_EULA Y
ENV SA_PASSWORD zaq1@WSX
# ENV MSSQL_PID pbdngr1
# run sudo apt-get update -y
WORKDIR /app
COPY . /app

RUN /opt/mssql/bin/sqlservr --accept-eula & sleep 30 \
    && /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P 'zaq1@WSX' -i /app/createdb.sql \
    && /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P 'zaq1@WSX' -i /app/createFunction.sql \
    && /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P 'zaq1@WSX' -i /app/createProcedures.sql \
    && /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P 'zaq1@WSX' -i /app/createConstraints.sql \
    && /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P 'zaq1@WSX' -i /app/createTriggers.sql \
    && /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P 'zaq1@WSX' -i /app/createViews.sql \
    && python3 /app/DataGenerator.py \
    && pkill sqlservr







