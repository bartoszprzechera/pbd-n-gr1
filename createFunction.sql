
USE pbd;
GO

CREATE FUNCTION dbo.get_product_price (@ProductId INT,@price_time datetime)
RETURNS FLOAT
AS
BEGIN
    return (SELECT dbo.Prices.NetPrice * (1 + dbo.Taxes.TaxPercentage / 100.0) AS Expr1 FROM dbo.Prices INNER JOIN dbo.Product ON dbo.Prices.ProductId = dbo.Product.ProductId INNER JOIN dbo.Taxes ON dbo.Prices.TaxId = dbo.Taxes.TaxId WHERE (dbo.Prices.StartDate <=@price_time) AND (dbo.Prices.EndDate >= @price_time) AND (dbo.Product.ProductId = @ProductId) )
END
go

CREATE FUNCTION get_order_item_price(@OrderItemId INT,@price_time datetime)
RETURNS FLOAT
AS
BEGIN
	return (SELECT dbo.Prices.NetPrice * (1 + dbo.Taxes.TaxPercentage / 100.0) * dbo.OrderItem.Quantity AS Expr1 FROM dbo.Prices INNER JOIN dbo.Product ON dbo.Prices.ProductId = dbo.Product.ProductId INNER JOIN dbo.Taxes ON dbo.Prices.TaxId = dbo.Taxes.TaxId INNER JOIN dbo.OrderItem ON dbo.Product.ProductId = dbo.OrderItem.ProductId WHERE        (dbo.Prices.StartDate <=@price_time) AND (dbo.Prices.EndDate >= @price_time) AND (dbo.OrderItem.OrderItemId = @OrderItemId))
END
GO

CREATE FUNCTION get_order_price(@OrderId INT)
RETURNS FLOAT
AS
BEGIN
	return (SELECT SUM((dbo.OrderItem.Quantity * dbo.Prices.NetPrice) * (1 + dbo.Taxes.TaxPercentage / 100.0)) AS Expr1 FROM dbo.Prices INNER JOIN dbo.Product ON dbo.Prices.ProductId = dbo.Product.ProductId INNER JOIN  dbo.Taxes ON dbo.Prices.TaxId = dbo.Taxes.TaxId INNER JOIN dbo.OrderItem ON dbo.Product.ProductId = dbo.OrderItem.ProductId INNER JOIN dbo.Orders ON dbo.OrderItem.OrderId = dbo.Orders.OrderId WHERE (dbo.Prices.StartDate <= dbo.Orders.OrderDate) AND (dbo.Prices.EndDate >= dbo.Orders.OrderDate) AND (dbo.Orders.OrderId = @OrderId) GROUP BY dbo.Orders.OrderId)
END
GO

CREATE FUNCTION get_client_orders_price(@ClientId INT)
RETURNS FLOAT
AS
BEGIN
	return (SELECT       SUM((dbo.OrderItem.Quantity * dbo.Prices.NetPrice) * (1 + dbo.Taxes.TaxPercentage / 100.0)) AS Expr1
FROM            dbo.Prices INNER JOIN
                         dbo.Product ON dbo.Prices.ProductId = dbo.Product.ProductId INNER JOIN
                         dbo.Taxes ON dbo.Prices.TaxId = dbo.Taxes.TaxId INNER JOIN
                         dbo.OrderItem ON dbo.Product.ProductId = dbo.OrderItem.ProductId INNER JOIN
                         dbo.Orders ON dbo.OrderItem.OrderId = dbo.Orders.OrderId INNER JOIN
                         dbo.Client ON dbo.Orders.ClientId = dbo.Client.ClientId
WHERE        (dbo.Prices.StartDate <= dbo.Orders.OrderDate) AND (dbo.Prices.EndDate >= dbo.Orders.OrderDate)
GROUP BY dbo.Client.ClientId
HAVING        (dbo.Client.ClientId = @ClientId))
END
GO

CREATE FUNCTION get_client_orders_count(@ClientId INT)
RETURNS INT
AS
BEGIN
return (SELECT        COUNT(dbo.Orders.OrderId) AS Expr2
FROM            dbo.Orders INNER JOIN
                         dbo.Client ON dbo.Orders.ClientId = dbo.Client.ClientId
WHERE        (dbo.Client.ClientId = @ClientId))
END
GO

CREATE FUNCTION get_free_RestaurationTables_in_restaurant(@RestaurantId INT,@table_time datetime)
RETURNS TABLE
AS
    RETURN (SELECT dbo.RestaurationTables.TableId FROM dbo.RestaurationTables WHERE dbo.RestaurationTables.TableId NOT IN (SELECT        dbo.RestaurationTables.TableId
FROM           dbo.RestaurationTables
						 LEFT JOIN  dbo.Orders ON dbo.Orders.TableId = dbo.RestaurationTables.TableId
WHERE        (dbo.RestaurationTables.RestaurantId = @RestaurantId) AND (dbo.Orders.OrderDate >  @table_time) AND (dbo.Orders.OrderDate < DATEADD(MI,90,@table_time)))
AND (dbo.RestaurationTables.RestaurantId = @RestaurantId))
GO

CREATE FUNCTION cant_item_be_added_to_menu(@MenuItemId INT,@MenuId int)
RETURNS bit
AS
BEGIN


return (
SELECT  count(dbo.MenuItem.ProductId) from  dbo.MenuItem where dbo.MenuItem.ProductId=@MenuItemId and  dbo.MenuItem.ProductId in (SELECT dbo.MenuItem.ProductId from MenuItem inner join dbo.Menu on dbo.Menu.MenuId = dbo.MenuItem.MenuId where dbo.Menu.MenuEndDate<=(select dbo.Menu.MenuStartDate from dbo.Menu where dbo.Menu.MenuId=@MenuItemId)and dbo.Menu.MenuEndDate>=DATEADD(dd,-7,(select dbo.Menu.MenuStartDate from dbo.Menu where dbo.Menu.MenuId=@MenuItemId)) and dbo.Menu.RestaurantId=(select dbo.Menu.RestaurantId from dbo.Menu where dbo.Menu.MenuId=@MenuItemId))

)
END
GO


CREATE FUNCTION is_table_ready_for_order(@RestaurantId INT,@tabletime datetime,@TableId int)
RETURNS bit
AS
BEGIN

IF @TableId is NULL
begin
    return (select 1)
end

return (select
count(dbo.RestaurationTables.TableId)
FROM dbo.RestaurationTables where dbo.RestaurationTables.TableId in (select * from dbo.get_free_RestaurationTables_in_restaurant(@RestaurantId,@tabletime)) and dbo.RestaurationTables.TableId=@TableId)
END
GO


CREATE FUNCTION can_sea_fruit_be_added(@reservation_creation_date datetime,@reservation_date datetime)
RETURNS bit
AS
BEGIN
IF (DATENAME(WEEKDAY,@reservation_date) = 'Thursday' AND DATEADD(DD,-2,@reservation_date)<@reservation_creation_date) or (DATENAME(WEEKDAY,@reservation_date) = 'Friday' AND DATEADD(DD,-3,@reservation_date)<@reservation_creation_date) OR (DATENAME(WEEKDAY,@reservation_date) = 'Saturday' AND DATEADD(DD,-4,@reservation_date)<@reservation_creation_date)
BEGIN
    RETURN (SELECT 1)
END
return(SELECT 0)


END
GO


CREATE FUNCTION can_order_item_be_added_if_it_is_sea_fruit(@ProductId int,@OrderId int)
RETURNS bit
AS
BEGIN
    DECLARE @ProductCategory INT;
    SET @ProductCategory = (select dbo.Product.CategoryId from dbo.Product where dbo.Product.ProductId=@ProductId);
    IF @ProductCategory != 1
    BEGIN
        return (select 1)
    END
    return (select 1)
    DECLARE @OrderDate datetime;
    DECLARE @ReservationId INT;
    DECLARE @ReservationDate datetime;
    SET @OrderDate = (select dbo.Orders.OrderDate from dbo.Orders where dbo.Orders.OrderId=@OrderId);
    SET @ReservationId =(select dbo.Reservation.ReservationId from dbo.Reservation where dbo.Reservation.OrderId=@OrderId);

    IF (select dbo.Reservation.ReservationId from dbo.Reservation where dbo.Reservation.OrderId=@OrderId) is null
    BEGIN
        return (select 0)
    END

    SET @ReservationDate = (select dbo.Reservation.ReservationDate from dbo.Reservation where dbo.Reservation.OrderId=@OrderId);
    return (select dbo.can_sea_fruit_be_added(@ReservationDate,@OrderDate))



END
GO

CREATE FUNCTION IsValidNip
(
  @nip nvarchar(15)
)
RETURNS bit
AS
BEGIN
  SELECT @nip = REPLACE(@nip,'-','')
  IF ISNUMERIC(@nip) = 0
    RETURN 0
  DECLARE
    @weights AS TABLE
    (
      Position tinyint IDENTITY(1,1) NOT NULL,
      Weight tinyint NOT NULL
    )
  INSERT INTO @weights VALUES (6), (5), (7), (2), (3), (4), (5), (6), (7)
  IF SUBSTRING(@nip, 10, 1) = (SELECT SUM(CONVERT(TINYINT, SUBSTRING(@nip, Position, 1)) * Weight) % 11 FROM @weights)
    RETURN 1
  RETURN 0
END
GO

CREATE FUNCTION cant_menu_be_added_data_collisions
(
  @RestaurantId INT, @MenuStartDate datetime,@MenuEndDate datetime
)
RETURNS bit
AS
BEGIN
    return (select count( dbo.Menu.MenuId) from dbo.Menu where dbo.Menu.RestaurantId=@RestaurantId AND ((dbo.Menu.MenuStartDate >@MenuStartDate and dbo.Menu.MenuStartDate <@MenuEndDate ) or (dbo.Menu.MenuEndDate >@MenuStartDate and dbo.Menu.MenuEndDate <@MenuEndDate ) or (dbo.Menu.MenuStartDate <@MenuStartDate and dbo.Menu.MenuEndDate >@MenuEndDate )))
END
GO

CREATE FUNCTION cant_price_be_added_data_collisions
(
  @ProductId INT, @StartDate datetime,@EndDate datetime
)
RETURNS bit
AS
BEGIN
    return (select count( dbo.Prices.ProductId) from dbo.Prices where dbo.Prices.ProductId=@ProductId AND ((dbo.Prices.StartDate >@StartDate and dbo.Prices.StartDate <@EndDate ) or (dbo.Prices.EndDate >@StartDate and dbo.Prices.EndDate <@EndDate ) or (dbo.Prices.StartDate <@StartDate and dbo.Prices.EndDate >@EndDate )))
END
GO

create FUNCTION isDiscountItemUsableByClient(@ClientId int, @use_time datetime,@DiscountItemId INT)
returns bit
as
begin
	IF @DiscountItemId is NULL
	begin
		return (select 1)
	end

	IF @ClientId is NULL
	BEGIN
	    return (select 0)
	END

    IF (select dbo.DiscountItem.ClientId from dbo.DiscountItem where dbo.DiscountItem.DiscountItemId= @DiscountItemId)!=@ClientId
    begin
        return (select 0)
    end

    declare @DiscountId INT
    set @DiscountId = (select dbo.DiscountItem.DiscountId from dbo.DiscountItem where dbo.DiscountItem.DiscountItemId= @DiscountItemId)

    declare @StartingDate date
    set @StartingDate = (select dbo.DiscountItem.StartingDate from dbo.DiscountItem where dbo.DiscountItem.DiscountItemId= @DiscountItemId)

    declare @NumOfDaysToUse INT
    set @NumOfDaysToUse = (select dbo.Discounts.NumOfDaysToUse from dbo.Discounts where dbo.Discounts.DiscountId= @DiscountId)

    IF @NumOfDaysToUse!=-1 and DATEADD(DD,@NumOfDaysToUse,@StartingDate)>@use_time
    begin
        return (select 0)
    end

    declare @IsOneTime INT
    set @IsOneTime = (select dbo.Discounts.IsOneTime from dbo.Discounts where dbo.Discounts.DiscountId= @DiscountId)

    IF @IsOneTime=1 and ((select count(dbo.Orders.OrderId) from dbo.Orders where dbo.Orders.DiscountItemId=@DiscountItemId)!=0 or (select count(dbo.Orders.OrderId) from dbo.Orders where dbo.Orders.DiscountItemId=@DiscountItemId) is not null )
    begin
        return (select 0)
    end

    return (select 1)
end
go


create FUNCTION canOrderItemBeAdded(@ProductId int, @OrderId int)
returns bit
as
BEGIN
    declare @OrderDate datetime
    set @OrderDate =  CAST((select dbo.Orders.OrderDate from dbo.Orders where dbo.Orders.OrderId=@OrderId)AS date)

    declare @RestaurantId int
    set @RestaurantId = (select dbo.Orders.RestaurantId from dbo.Orders where dbo.Orders.OrderId=@OrderId)

    declare @MenuId int
    set @MenuId = (select dbo.Menu.MenuId from dbo.Menu where dbo.Menu.RestaurantId=@RestaurantId and dbo.Menu.MenuStartDate<=@OrderDate and dbo.Menu.MenuEndDate>=@OrderDate)

    IF (select dbo.MenuItem.ProductId from dbo.MenuItem where dbo.MenuItem.MenuId=@MenuId and dbo.MenuItem.ProductId=@ProductId )>0
    begin
        return ( select 1)
    end
        return (select 0)


END
GO



create FUNCTION isNumberOfPersonsLessThanTableLimit(@TableId int, @NumberOfPersons int)
returns bit
as
BEGIN
    IF @TableId IS NULL OR @NumberOfPersons=0
    BEGIN
        return (select 1)
    END
    declare @table_limit int
    set @table_limit = (select dbo.RestaurationTables.Capacity from dbo.RestaurationTables where dbo.RestaurationTables.TableId=@TableId)

    IF @table_limit>=@NumberOfPersons
    begin
        return (select 1)
    end
    return (select 0)
END
GO


--create FUNCTION canTableBeAddedToResturant(@Capacity int, @RestaurantId int)
--returns bit
--as
--BEGIN
--    declare @seats_limit int
--    set @seats_limit = (select dbo.Restaurant.maximalPeopleAtOnce from dbo.Restaurant where dbo.Restaurant.RestaurantId=@RestaurantId)
--
--    declare @actual_num_of_seats int
--    set @actual_num_of_seats = (select @seats_limit-sum(dbo.RestaurationTables.Capacity)-@Capacity from dbo.RestaurationTables where dbo.RestaurationTables.RestaurantId=@RestaurantId )
--
--    return (select @actual_num_of_seats)
--
--END
--GO


create FUNCTION canClientMakeReservation(@OrderId Int)
returns bit
as
BEGIN
    declare @ClientId int
    set @ClientId= (select dbo.Orders.ClientId from dbo.Orders where dbo.Orders.OrderId=@OrderId)

    IF (dbo.get_client_orders_count(@ClientId)>=5 or dbo.get_client_orders_price(@ClientId)>=200) and dbo.get_order_price(@OrderId)>=50 or dbo.get_order_price(@OrderId)is not null
    BEGIN
    return (select 1)
    END
    return (select 0)
END
GO
