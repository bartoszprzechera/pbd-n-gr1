# PBD N gr1

## Projekt bazy danych mssql-server

**Useful links**
1. [Asana board](https://app.asana.com/0/1200026174469389/board)
3. [Upel page](https://upel2.cel.agh.edu.pl/wiet/course/view.php?id=1306)
4. [YT channel about DB](https://www.youtube.com/channel/UCg6KcsucpULST6rs--BNTkg)
5. [Markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
6. [Schemat bazy](https://app.sqldbm.com/SQLServer/Edit/p158433/#)

**SQL tuts**
1. [SQL tutorial z upela](https://upel2.cel.agh.edu.pl/wiet/mod/folder/view.php?id=39994)
2. [postgresqltutorial](https://www.postgresqltutorial.com/)
3. [Mike Dane tutorial](https://www.mikedane.com/databases/sql/)

**Dependencies**

1. [Docker image](https://hub.docker.com/_/microsoft-mssql-server)

## How to run
firstly clone repo and move to cloned directory:
````shell script
git clone https://gitlab.com/bartoszprzechera/pbd-n-gr1.git
cd pbd-n-gr1
````
secondly build a container:
````shell script
docker build -t "pbd" .
````
finally run container with server
````shell script
docker run -p 1433:1433 -d pbd
```` 


